_**Team 9**_
| Name                  | Email                        | ID        | Gitlab profile                |
| :---                  | :---                         | :---      | :---                          |
| Juan Pablo Correa     | jp.correap@uniandes.edu.co   | 201717404 | @jpcorreap :tropical_fish:    |
| Juan Camilo Garcia    | jc.garciar1@uniandes.edu.co  | 201729554 | @jcgarciar1 :100:             |
| Luis Miguel Gómez     | lm.gomezl@uniandes.edu.co    | 201729597 | @luismigolondo :monkey:       |
| Diego Andrés Gómez    | da.gomezp@uniandes.edu.co    | 201713198 | @diegommezp28 :ok_hand_tone2: |
| Juan Antonio Restrepo | ja.restrepok@uniandes.edu.co | 201730269 | @juanresk :dragon_face:       |
| Isabela Sarmiento     | im.sarmiento@uniandes.edu.co | 201731759 | @imsarmiento :gem:            |


***
_**Table of content**_

_**Sprint 1**_

|  |  |
| ------ | ------ |
| MS1 | [MS1 Wiki](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/MS1) |
| MS2 | [MS2 Wiki](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/MS2) |
| MS3 | [MS3 Wiki](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/MS3) |
| MS4 | [MS4 Wiki](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/MS4) |
| Final Report | [Final Report](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/Final-Report) |

**Sprint 2**

|  |  |
| ------ | ------ |
| Final Report  | [Final Report](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/Final-Report-Sprint-2) |



**Sprint 3**

|  |  |
| ------ | ------ |
| MS10 | [MS10 Wiki](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/MS10) |
| Feedback Sprint 3 | [Feedback Sprint 3](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/Feedback-sprint-3) |
| Final Report Sprint 3 | [Final Report Sprint 3](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/Final-Report-Sprint-3) |

**Sprint 4**

|  |  |
| ------ | ------ |
| Final Report  | [Final Report](https://gitlab.com/isis3510_202110_team9/wiki/-/wikis/Final-Report-Sprint-4) |



